﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Description" Type="Str">This class is forseen to inspect the PHELIX beamline and to select the mode. Additional several settings can be chosen.

This class is part of the PHELIX user layer.

author: Stefan Götte, GSI

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation; either version 2 of 
the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 18-JUN-2008</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)X!!!*Q(C=\&gt;3^4=.1&amp;)&lt;B,YC#.BOAC!X/#BG"#,(!7=&amp;NSKRQ"EDD.K,+#K?BJ-A+7=']PDZ"15+%!C3%M(-&gt;Z\N`4`RTJ&lt;&lt;&gt;3.=;,GX[&gt;+._&lt;.*+@?LQ6D'=:=.Q/J^/TPJ`50__``#&amp;`F06_`EP_@`L`_P`=0XFVXN^M&gt;%0&lt;^,M^(YH*3B/M?E.4P)E4`)E4`)E$`)A$`)A$`)A&gt;X)H&gt;X)H&gt;X)H.X)D.X)D.X)\L68E)B?ZL+UMS?4*2-GAS1"*9SB+$IEH]33?R-.0*:\%EXA34_+BC2*0YEE]C3@RU%W**`%EHM34?"CK3&lt;,NZ(A3$]-L]!3?Q".Y!A^4+P!%A'#S9/"A%"A++I/4Q".Y!A_H#DS"*`!%HM"$N1*0Y!E]A3@QU+6&gt;F7C;=3@(QT"S0)\(]4A?R]01=DS/R`%Y(M@$&gt;()]DM&gt;"/"-[AU/1U]FJY0RQ0)[(,TE?R_.Y()`DI;L&gt;)7^8:N3-/TE?QW.Y$)`B-4Q-)=.D?!S0Y4%]$#P$9XA-D_%R0%QFQW.Y$)]"-3:F?BG$'2W.2E:A?0CUJ]8;89IGM&lt;&lt;8KTE^K+I(506AK2Y9V9/AOM'K'[?[);I,L&lt;K!KAOD_M/K0[)#KC:7$;BKK#0(!W60[3E&lt;3E&gt;:5:;5"75_&gt;PXGBM@D59@$1@P^8HX@;\0:K/M[L69L,:&gt;,,29,T?@TN^8KFHX;:K&gt;V;=XZ\G\\W$^MHX=0WZ?_Y`P_[8(-@\*-[^*P7"NVJ?&amp;MG?=;P1*,QEQC!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_Beamline.i attribute.ctl" Type="VI" URL="../PH_Beamline.i attribute.ctl"/>
		<Item Name="PH_Beamline.i attribute.vi" Type="VI" URL="../PH_Beamline.i attribute.vi"/>
		<Item Name="PH_Beamline.PC _Picture Condition Services.vi" Type="VI" URL="../PH_Beamline.PC _Picture Condition Services.vi"/>
		<Item Name="PH_Beamline.PC _R2S.vi" Type="VI" URL="../PH_Beamline.PC _R2S.vi"/>
		<Item Name="PH_Beamline.PC _Settings.vi" Type="VI" URL="../PH_Beamline.PC _Settings.vi"/>
		<Item Name="PH_Beamline.ProcEvents.vi" Type="VI" URL="../PH_Beamline.ProcEvents.vi"/>
		<Item Name="PH_Beamline.set Comp Vacuum depending GUI elements.vi" Type="VI" URL="../PH_Beamline.set Comp Vacuum depending GUI elements.vi"/>
		<Item Name="PH_Beamline.set Experiment Shutter Information.vi" Type="VI" URL="../PH_Beamline.set Experiment Shutter Information.vi"/>
		<Item Name="PH_Beamline.set GUI elements.vi" Type="VI" URL="../PH_Beamline.set GUI elements.vi"/>
		<Item Name="PH_Beamline.set i Attr.vi" Type="VI" URL="../PH_Beamline.set i Attr.vi"/>
		<Item Name="PH_Beamline.set PA DP depending GUI elements.vi" Type="VI" URL="../PH_Beamline.set PA DP depending GUI elements.vi"/>
		<Item Name="PH_Beamline.set Radiation Shutter Settings.vi" Type="VI" URL="../PH_Beamline.set Radiation Shutter Settings.vi"/>
		<Item Name="PH_Beamline.set start page.vi" Type="VI" URL="../PH_Beamline.set start page.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PH_Beamline.get i attribute.vi" Type="VI" URL="../PH_Beamline.get i attribute.vi"/>
		<Item Name="PH_Beamline.ProcCases.vi" Type="VI" URL="../PH_Beamline.ProcCases.vi"/>
		<Item Name="PH_Beamline.ProcPeriodic.vi" Type="VI" URL="../PH_Beamline.ProcPeriodic.vi"/>
		<Item Name="PH_Beamline.set GUI Refs.vi" Type="VI" URL="../PH_Beamline.set GUI Refs.vi"/>
		<Item Name="PH_Beamline.set i attribute.vi" Type="VI" URL="../PH_Beamline.set i attribute.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="PH_Beamline.constructor.vi" Type="VI" URL="../PH_Beamline.constructor.vi"/>
		<Item Name="PH_Beamline.destructor.vi" Type="VI" URL="../PH_Beamline.destructor.vi"/>
		<Item Name="PH_Beamline.get data to modify.vi" Type="VI" URL="../PH_Beamline.get data to modify.vi"/>
		<Item Name="PH_Beamline.GUI Refs.ctl" Type="VI" URL="../PH_Beamline.GUI Refs.ctl"/>
		<Item Name="PH_Beamline.panel.vi" Type="VI" URL="../PH_Beamline.panel.vi"/>
		<Item Name="PH_Beamline.set modified data.vi" Type="VI" URL="../PH_Beamline.set modified data.vi"/>
	</Item>
	<Item Name="PH_Beamline.contents.vi" Type="VI" URL="../PH_Beamline.contents.vi"/>
</Library>
