﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_BL-Master.i attribute.ctl" Type="VI" URL="../PH_BL-Master.i attribute.ctl"/>
		<Item Name="PH_BL-Master.i attribute.vi" Type="VI" URL="../PH_BL-Master.i attribute.vi"/>
		<Item Name="PH_BL-Master.indicate O2 State.vi" Type="VI" URL="../PH_BL-Master.indicate O2 State.vi"/>
		<Item Name="PH_BL-Master.PC _getO2Conctr.vi" Type="VI" URL="../PH_BL-Master.PC _getO2Conctr.vi"/>
		<Item Name="PH_BL-Master.PC _getO2DevStatus.vi" Type="VI" URL="../PH_BL-Master.PC _getO2DevStatus.vi"/>
		<Item Name="PH_BL-Master.PC _getVacLimits.vi" Type="VI" URL="../PH_BL-Master.PC _getVacLimits.vi"/>
		<Item Name="PH_BL-Master.PC _getVacPressuress.vi" Type="VI" URL="../PH_BL-Master.PC _getVacPressuress.vi"/>
		<Item Name="PH_BL-Master.PC calc Vac State.vi" Type="VI" URL="../PH_BL-Master.PC calc Vac State.vi"/>
		<Item Name="PH_BL-Master.set vacuum Gui element tip strips.vi" Type="VI" URL="../PH_BL-Master.set vacuum Gui element tip strips.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_BL-Master.get i attribute.vi" Type="VI" URL="../PH_BL-Master.get i attribute.vi"/>
		<Item Name="PH_BL-Master.ProcCases.vi" Type="VI" URL="../PH_BL-Master.ProcCases.vi"/>
		<Item Name="PH_BL-Master.ProcEvents.vi" Type="VI" URL="../PH_BL-Master.ProcEvents.vi"/>
		<Item Name="PH_BL-Master.ProcPeriodic.vi" Type="VI" URL="../PH_BL-Master.ProcPeriodic.vi"/>
		<Item Name="PH_BL-Master.set GUI Refs.vi" Type="VI" URL="../PH_BL-Master.set GUI Refs.vi"/>
		<Item Name="PH_BL-Master.set i attribute.vi" Type="VI" URL="../PH_BL-Master.set i attribute.vi"/>
		<Item Name="PH_BL-Master.set Subscriber.vi" Type="VI" URL="../PH_BL-Master.set Subscriber.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_BL-Master.constructor.vi" Type="VI" URL="../PH_BL-Master.constructor.vi"/>
		<Item Name="PH_BL-Master.destructor.vi" Type="VI" URL="../PH_BL-Master.destructor.vi"/>
		<Item Name="PH_BL-Master.get data to modify.vi" Type="VI" URL="../PH_BL-Master.get data to modify.vi"/>
		<Item Name="PH_BL-Master.GUI Refs.ctl" Type="VI" URL="../PH_BL-Master.GUI Refs.ctl"/>
		<Item Name="PH_BL-Master.set modified data.vi" Type="VI" URL="../PH_BL-Master.set modified data.vi"/>
	</Item>
	<Item Name="PH_BL-Master.contents.vi" Type="VI" URL="../PH_BL-Master.contents.vi"/>
</Library>
