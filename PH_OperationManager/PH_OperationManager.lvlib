﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_OperationManager.i attribute.ctl" Type="VI" URL="../PH_OperationManager.i attribute.ctl"/>
		<Item Name="PH_OperationManager.i attribute.vi" Type="VI" URL="../PH_OperationManager.i attribute.vi"/>
		<Item Name="PH_OperationManager.PC _Status Service.vi" Type="VI" URL="../PH_OperationManager.PC _Status Service.vi"/>
		<Item Name="PH_OperationManager.PC Powermeter Ranges.vi" Type="VI" URL="../PH_OperationManager.PC Powermeter Ranges.vi"/>
		<Item Name="PH_OperationManager.PC Write PSDB.vi" Type="VI" URL="../PH_OperationManager.PC Write PSDB.vi"/>
		<Item Name="PH_OperationManager.ProcEvents.vi" Type="VI" URL="../PH_OperationManager.ProcEvents.vi"/>
		<Item Name="PH_OperationManager.set 4Q Visibility.vi" Type="VI" URL="../PH_OperationManager.set 4Q Visibility.vi"/>
		<Item Name="PH_OperationManager.set Attr and open Services.vi" Type="VI" URL="../PH_OperationManager.set Attr and open Services.vi"/>
		<Item Name="PH_OperationManager.set Beamline Settings.vi" Type="VI" URL="../PH_OperationManager.set Beamline Settings.vi"/>
		<Item Name="PH_OperationManager.set BL Elements Visibility.vi" Type="VI" URL="../PH_OperationManager.set BL Elements Visibility.vi"/>
		<Item Name="PH_OperationManager.set Delay to GUI.vi" Type="VI" URL="../PH_OperationManager.set Delay to GUI.vi"/>
		<Item Name="PH_OperationManager.set Ranges Visibility.vi" Type="VI" URL="../PH_OperationManager.set Ranges Visibility.vi"/>
		<Item Name="PH_OperationManager.set Revolver Visibility.vi" Type="VI" URL="../PH_OperationManager.set Revolver Visibility.vi"/>
		<Item Name="PH_OperationManager.set Visibility.vi" Type="VI" URL="../PH_OperationManager.set Visibility.vi"/>
		<Item Name="PH_OperationManager.set Waveplate Visibility.vi" Type="VI" URL="../PH_OperationManager.set Waveplate Visibility.vi"/>
		<Item Name="PH_OperationManager.set Z6 Beamline Settings.vi" Type="VI" URL="../PH_OperationManager.set Z6 Beamline Settings.vi"/>
		<Item Name="PH_OperationManager.set Z6 Elements Visibility.vi" Type="VI" URL="../PH_OperationManager.set Z6 Elements Visibility.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_OperationManager.get i attribute.vi" Type="VI" URL="../PH_OperationManager.get i attribute.vi"/>
		<Item Name="PH_OperationManager.ProcCases.vi" Type="VI" URL="../PH_OperationManager.ProcCases.vi"/>
		<Item Name="PH_OperationManager.ProcPeriodic.vi" Type="VI" URL="../PH_OperationManager.ProcPeriodic.vi"/>
		<Item Name="PH_OperationManager.set i attribute.vi" Type="VI" URL="../PH_OperationManager.set i attribute.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_OperationManager.constructor.vi" Type="VI" URL="../PH_OperationManager.constructor.vi"/>
		<Item Name="PH_OperationManager.destructor.vi" Type="VI" URL="../PH_OperationManager.destructor.vi"/>
		<Item Name="PH_OperationManager.evt call Clear Beamline.vi" Type="VI" URL="../PH_OperationManager.evt call Clear Beamline.vi"/>
		<Item Name="PH_OperationManager.evt call Clear Z6 Beamline.vi" Type="VI" URL="../PH_OperationManager.evt call Clear Z6 Beamline.vi"/>
		<Item Name="PH_OperationManager.evt call Main Off.vi" Type="VI" URL="../PH_OperationManager.evt call Main Off.vi"/>
		<Item Name="PH_OperationManager.get data to modify.vi" Type="VI" URL="../PH_OperationManager.get data to modify.vi"/>
		<Item Name="PH_OperationManager.panel.vi" Type="VI" URL="../PH_OperationManager.panel.vi"/>
		<Item Name="PH_OperationManager.set modified data.vi" Type="VI" URL="../PH_OperationManager.set modified data.vi"/>
	</Item>
	<Item Name="PH_OperationManager.contents.vi" Type="VI" URL="../PH_OperationManager.contents.vi"/>
</Library>
