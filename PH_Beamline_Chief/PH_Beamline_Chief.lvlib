﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_Beamline_Chief.Calc BL ALL State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc BL ALL State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc Booster State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc Booster State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc Compressor State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc Compressor State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc Experiment LB State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc Experiment LB State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc Experiment LL State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc Experiment LL State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc fs State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc fs State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc fs-FE DP State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc fs-FE DP State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc InjBox State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc InjBox State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc MA State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc MA State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc MAS State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc MAS State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc Mirrortower State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc Mirrortower State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc ns State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc ns State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc PA ED State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc PA ED State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc PA State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc PA State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc State.vi"/>
		<Item Name="PH_Beamline_Chief.Calc uOPA State.vi" Type="VI" URL="../PH_Beamline_Chief.Calc uOPA State.vi"/>
		<Item Name="PH_Beamline_Chief.color Beamline.vi" Type="VI" URL="../PH_Beamline_Chief.color Beamline.vi"/>
		<Item Name="PH_Beamline_Chief.color Booster BL.vi" Type="VI" URL="../PH_Beamline_Chief.color Booster BL.vi"/>
		<Item Name="PH_Beamline_Chief.color Exp LB BL.vi" Type="VI" URL="../PH_Beamline_Chief.color Exp LB BL.vi"/>
		<Item Name="PH_Beamline_Chief.color fs-FE BL.vi" Type="VI" URL="../PH_Beamline_Chief.color fs-FE BL.vi"/>
		<Item Name="PH_Beamline_Chief.color fs-FE DP BL.vi" Type="VI" URL="../PH_Beamline_Chief.color fs-FE DP BL.vi"/>
		<Item Name="PH_Beamline_Chief.color LL BL.vi" Type="VI" URL="../PH_Beamline_Chief.color LL BL.vi"/>
		<Item Name="PH_Beamline_Chief.color MA BL.vi" Type="VI" URL="../PH_Beamline_Chief.color MA BL.vi"/>
		<Item Name="PH_Beamline_Chief.color Mirrortower BL.vi" Type="VI" URL="../PH_Beamline_Chief.color Mirrortower BL.vi"/>
		<Item Name="PH_Beamline_Chief.color ns-FE BL.vi" Type="VI" URL="../PH_Beamline_Chief.color ns-FE BL.vi"/>
		<Item Name="PH_Beamline_Chief.color PA BL.vi" Type="VI" URL="../PH_Beamline_Chief.color PA BL.vi"/>
		<Item Name="PH_Beamline_Chief.color PA Input BL.vi" Type="VI" URL="../PH_Beamline_Chief.color PA Input BL.vi"/>
		<Item Name="PH_Beamline_Chief.Event Call Set Settings.vi" Type="VI" URL="../PH_Beamline_Chief.Event Call Set Settings.vi"/>
		<Item Name="PH_Beamline_Chief.get Section State.vi" Type="VI" URL="../PH_Beamline_Chief.get Section State.vi"/>
		<Item Name="PH_Beamline_Chief.get Unit R2S.vi" Type="VI" URL="../PH_Beamline_Chief.get Unit R2S.vi"/>
		<Item Name="PH_Beamline_Chief.get Vacuum State.vi" Type="VI" URL="../PH_Beamline_Chief.get Vacuum State.vi"/>
		<Item Name="PH_Beamline_Chief.i attribute.ctl" Type="VI" URL="../PH_Beamline_Chief.i attribute.ctl"/>
		<Item Name="PH_Beamline_Chief.i attribute.vi" Type="VI" URL="../PH_Beamline_Chief.i attribute.vi"/>
		<Item Name="PH_Beamline_Chief.Laser Off Activity.vi" Type="VI" URL="../PH_Beamline_Chief.Laser Off Activity.vi"/>
		<Item Name="PH_Beamline_Chief.PC Clear Beamline.vi" Type="VI" URL="../PH_Beamline_Chief.PC Clear Beamline.vi"/>
		<Item Name="PH_Beamline_Chief.PC Set Settings.vi" Type="VI" URL="../PH_Beamline_Chief.PC Set Settings.vi"/>
		<Item Name="PH_Beamline_Chief.ProcEvents.vi" Type="VI" URL="../PH_Beamline_Chief.ProcEvents.vi"/>
		<Item Name="PH_Beamline_Chief.react on Settings.vi" Type="VI" URL="../PH_Beamline_Chief.react on Settings.vi"/>
		<Item Name="PH_Beamline_Chief.set i Attr and create DIM Services.vi" Type="VI" URL="../PH_Beamline_Chief.set i Attr and create DIM Services.vi"/>
		<Item Name="PH_Beamline_Chief.set Parents Attributes.vi" Type="VI" URL="../PH_Beamline_Chief.set Parents Attributes.vi"/>
		<Item Name="PH_Beamline_Chief.set Section State.vi" Type="VI" URL="../PH_Beamline_Chief.set Section State.vi"/>
		<Item Name="PH_Beamline_Chief.set Unit R2S.vi" Type="VI" URL="../PH_Beamline_Chief.set Unit R2S.vi"/>
		<Item Name="PH_Beamline_Chief.Settings 2 Data String.vi" Type="VI" URL="../PH_Beamline_Chief.Settings 2 Data String.vi"/>
		<Item Name="PH_Beamline_Chief.switch Exp Shutters.vi" Type="VI" URL="../PH_Beamline_Chief.switch Exp Shutters.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_Beamline_Chief.get i attribute.vi" Type="VI" URL="../PH_Beamline_Chief.get i attribute.vi"/>
		<Item Name="PH_Beamline_Chief.ProcCases.vi" Type="VI" URL="../PH_Beamline_Chief.ProcCases.vi"/>
		<Item Name="PH_Beamline_Chief.ProcPeriodic.vi" Type="VI" URL="../PH_Beamline_Chief.ProcPeriodic.vi"/>
		<Item Name="PH_Beamline_Chief.set i attribute.vi" Type="VI" URL="../PH_Beamline_Chief.set i attribute.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_Beamline_Chief.constructor.vi" Type="VI" URL="../PH_Beamline_Chief.constructor.vi"/>
		<Item Name="PH_Beamline_Chief.Data to Settings.vi" Type="VI" URL="../PH_Beamline_Chief.Data to Settings.vi"/>
		<Item Name="PH_Beamline_Chief.destructor.vi" Type="VI" URL="../PH_Beamline_Chief.destructor.vi"/>
		<Item Name="PH_Beamline_Chief.Evt Call clear Beamline.vi" Type="VI" URL="../PH_Beamline_Chief.Evt Call clear Beamline.vi"/>
		<Item Name="PH_Beamline_Chief.get data to modify.vi" Type="VI" URL="../PH_Beamline_Chief.get data to modify.vi"/>
		<Item Name="PH_Beamline_Chief.get Section Status Tagname.vi" Type="VI" URL="../PH_Beamline_Chief.get Section Status Tagname.vi"/>
		<Item Name="PH_Beamline_Chief.get Unit R2S Tagname.vi" Type="VI" URL="../PH_Beamline_Chief.get Unit R2S Tagname.vi"/>
		<Item Name="PH_Beamline_Chief.Motion GUI Instances.ctl" Type="VI" URL="../PH_Beamline_Chief.Motion GUI Instances.ctl"/>
		<Item Name="PH_Beamline_Chief.panel.vi" Type="VI" URL="../PH_Beamline_Chief.panel.vi"/>
		<Item Name="PH_Beamline_Chief.set modified data.vi" Type="VI" URL="../PH_Beamline_Chief.set modified data.vi"/>
		<Item Name="PH_Beamline_Chief.Settings.ctl" Type="VI" URL="../PH_Beamline_Chief.Settings.ctl"/>
		<Item Name="PH_Beamline_Chief.Unit.ctl" Type="VI" URL="../PH_Beamline_Chief.Unit.ctl"/>
	</Item>
	<Item Name="PH_Beamline_Chief.contents.vi" Type="VI" URL="../PH_Beamline_Chief.contents.vi"/>
</Library>
