﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="Z6_Beamline_Chief.Calc 100TW COS State.vi" Type="VI" URL="../Z6_Beamline_Chief.Calc 100TW COS State.vi"/>
		<Item Name="Z6_Beamline_Chief.Calc BL ALL State.vi" Type="VI" URL="../Z6_Beamline_Chief.Calc BL ALL State.vi"/>
		<Item Name="Z6_Beamline_Chief.Calc Northtower State.vi" Type="VI" URL="../Z6_Beamline_Chief.Calc Northtower State.vi"/>
		<Item Name="Z6_Beamline_Chief.Calc Southtower State.vi" Type="VI" URL="../Z6_Beamline_Chief.Calc Southtower State.vi"/>
		<Item Name="Z6_Beamline_Chief.Calc State.vi" Type="VI" URL="../Z6_Beamline_Chief.Calc State.vi"/>
		<Item Name="Z6_Beamline_Chief.get Unit R2S.vi" Type="VI" URL="../Z6_Beamline_Chief.get Unit R2S.vi"/>
		<Item Name="Z6_Beamline_Chief.i attribute.ctl" Type="VI" URL="../Z6_Beamline_Chief.i attribute.ctl"/>
		<Item Name="Z6_Beamline_Chief.i attribute.vi" Type="VI" URL="../Z6_Beamline_Chief.i attribute.vi"/>
		<Item Name="Z6_Beamline_Chief.PC Clear Beamline.vi" Type="VI" URL="../Z6_Beamline_Chief.PC Clear Beamline.vi"/>
		<Item Name="Z6_Beamline_Chief.ProcEvents.vi" Type="VI" URL="../Z6_Beamline_Chief.ProcEvents.vi"/>
		<Item Name="Z6_Beamline_Chief.set i Attr and create DIM Services.vi" Type="VI" URL="../Z6_Beamline_Chief.set i Attr and create DIM Services.vi"/>
		<Item Name="Z6_Beamline_Chief.set Parents Attributes.vi" Type="VI" URL="../Z6_Beamline_Chief.set Parents Attributes.vi"/>
		<Item Name="Z6_Beamline_Chief.set Unit R2S.vi" Type="VI" URL="../Z6_Beamline_Chief.set Unit R2S.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="Z6_Beamline_Chief.get i attribute.vi" Type="VI" URL="../Z6_Beamline_Chief.get i attribute.vi"/>
		<Item Name="Z6_Beamline_Chief.ProcCases.vi" Type="VI" URL="../Z6_Beamline_Chief.ProcCases.vi"/>
		<Item Name="Z6_Beamline_Chief.ProcPeriodic.vi" Type="VI" URL="../Z6_Beamline_Chief.ProcPeriodic.vi"/>
		<Item Name="Z6_Beamline_Chief.set i attribute.vi" Type="VI" URL="../Z6_Beamline_Chief.set i attribute.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="Z6_Beamline_Chief.constructor.vi" Type="VI" URL="../Z6_Beamline_Chief.constructor.vi"/>
		<Item Name="Z6_Beamline_Chief.destructor.vi" Type="VI" URL="../Z6_Beamline_Chief.destructor.vi"/>
		<Item Name="Z6_Beamline_Chief.Evt Call clear Beamline.vi" Type="VI" URL="../Z6_Beamline_Chief.Evt Call clear Beamline.vi"/>
		<Item Name="Z6_Beamline_Chief.get data to modify.vi" Type="VI" URL="../Z6_Beamline_Chief.get data to modify.vi"/>
		<Item Name="Z6_Beamline_Chief.get Unit R2S Tagname.vi" Type="VI" URL="../Z6_Beamline_Chief.get Unit R2S Tagname.vi"/>
		<Item Name="Z6_Beamline_Chief.Motion GUI Instances.ctl" Type="VI" URL="../Z6_Beamline_Chief.Motion GUI Instances.ctl"/>
		<Item Name="Z6_Beamline_Chief.panel.vi" Type="VI" URL="../Z6_Beamline_Chief.panel.vi"/>
		<Item Name="Z6_Beamline_Chief.set modified data.vi" Type="VI" URL="../Z6_Beamline_Chief.set modified data.vi"/>
		<Item Name="Z6_Beamline_Chief.Unit.ctl" Type="VI" URL="../Z6_Beamline_Chief.Unit.ctl"/>
	</Item>
	<Item Name="Z6_Beamline_Chief.contents.vi" Type="VI" URL="../Z6_Beamline_Chief.contents.vi"/>
</Library>
